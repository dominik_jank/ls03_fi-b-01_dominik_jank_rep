package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

public class Form_aendern extends JFrame {

	private JPanel contentPane;
	private JTextField txtHierBitteText;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Form_aendern frame = new Form_aendern();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Form_aendern() {
		setTitle("Form_aendern");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 386, 512);
		contentPane = new JPanel();
		contentPane.setName("");
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel label_bsptext = new JLabel("Dieser Text soll ver\u00E4ndert werden.");
		label_bsptext.setHorizontalAlignment(SwingConstants.CENTER);
		label_bsptext.setBounds(10, 22, 356, 14);
		contentPane.add(label_bsptext);

		JLabel label_aufg1 = new JLabel("Aufgabe 1: Hintergrundfarbe \u00E4ndern");
		label_aufg1.setBounds(10, 57, 186, 14);
		contentPane.add(label_aufg1);

		JLabel label_aufg2 = new JLabel("Aufgabe 2: Text formatieren");
		label_aufg2.setBounds(10, 134, 186, 14);
		contentPane.add(label_aufg2);

		JLabel label_aufg3 = new JLabel("Aufgabe 3: Schriftfarbe \u00E4ndern");
		label_aufg3.setBounds(10, 249, 186, 14);
		contentPane.add(label_aufg3);

		JLabel label_aufg4 = new JLabel("Aufgabe 4: Schriftgr\u00F6\u00DFe ver\u00E4ndern");
		label_aufg4.setBounds(10, 304, 186, 14);
		contentPane.add(label_aufg4);

		JLabel label_aufg5 = new JLabel("Aufgabe 5: Textausrichtung");
		label_aufg5.setBounds(10, 363, 186, 14);
		contentPane.add(label_aufg5);

		JLabel label_aufg6 = new JLabel("Aufgabe 6: Programm beenden");
		label_aufg6.setBounds(10, 424, 186, 14);
		contentPane.add(label_aufg6);

		JButton btnRotB = new JButton("Rot");
		btnRotB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnRotB_clicked();
			}

			private void btnRotB_clicked() {
				contentPane.setBackground(Color.RED);

			}
		});
		btnRotB.setBounds(10, 76, 112, 23);
		contentPane.add(btnRotB);

		JButton btnGelbB = new JButton("Gelb");
		btnGelbB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnGelbB_clicked();
			}

			private void btnGelbB_clicked() {
				contentPane.setBackground(Color.yellow);

			}
		});
		btnGelbB.setBounds(10, 98, 112, 23);
		contentPane.add(btnGelbB);

		JButton btnGreenB = new JButton("Gr\u00FCn");
		btnGreenB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnGreenB_clicked();
			}

			private void btnGreenB_clicked() {
				contentPane.setBackground(Color.green);

			}
		});
		btnGreenB.setBounds(132, 76, 112, 23);
		contentPane.add(btnGreenB);

		JButton btnStandardB = new JButton("Standard");
		btnStandardB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnStandardB_clicked();
			}

			private void btnStandardB_clicked() {
				contentPane.setBackground(null);
			}
		});
		btnStandardB.setBounds(132, 100, 112, 23);
		contentPane.add(btnStandardB);

		JButton btnBlauB = new JButton("Blau");
		btnBlauB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnBlueB_clicked();
			}

			private void btnBlueB_clicked() {
				contentPane.setBackground(Color.blue);
			}
		});
		btnBlauB.setBounds(254, 76, 112, 23);
		contentPane.add(btnBlauB);

		JButton btnFarbeWahlB = new JButton("Farbwahl");
		btnFarbeWahlB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnFarbeWahlB_clicked();
			}

			private void btnFarbeWahlB_clicked() {
				JColorChooser colorChooser = new JColorChooser();
				Color newColor = JColorChooser.showDialog(null, "Choose a color", null);
				contentPane.setBackground(newColor);
			}
		});
		btnFarbeWahlB.setBounds(254, 98, 112, 23);
		contentPane.add(btnFarbeWahlB);

		JButton btnArial = new JButton("Arial");
		btnArial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnArial_clicked();
			}

			private void btnArial_clicked() {
				Font schriftart = new Font("Arial", label_bsptext.getFont().getStyle(), 11);
				label_bsptext.setFont(schriftart);
			}
		});
		btnArial.setBounds(10, 150, 112, 23);
		contentPane.add(btnArial);

		JButton btnComicSansMs = new JButton("Comic Sans MS");
		btnComicSansMs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnComicSansMs_clicked();
			}

			private void btnComicSansMs_clicked() {
				Font schriftart = new Font("Comic Sans MS", label_bsptext.getFont().getStyle(), 11);
				label_bsptext.setFont(schriftart);
			}
		});
		btnComicSansMs.setBounds(131, 150, 113, 23);
		contentPane.add(btnComicSansMs);

		JButton btnCourierNew = new JButton("Courier New");
		btnCourierNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnCourierNew_clicked();
			}

			private void btnCourierNew_clicked() {
				Font schriftart = new Font("Courier New", label_bsptext.getFont().getStyle(), 11);
				label_bsptext.setFont(schriftart);
			}
		});
		btnCourierNew.setBounds(254, 150, 112, 23);
		contentPane.add(btnCourierNew);

		txtHierBitteText = new JTextField();
		txtHierBitteText.setText("Hier bitte Text eingeben");

		txtHierBitteText.setBounds(10, 184, 356, 20);
		contentPane.add(txtHierBitteText);
		txtHierBitteText.setColumns(10);

		JButton btnInsLabelSchreiben = new JButton("Ins Label schreiben");
		btnInsLabelSchreiben.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnInsLabelSchreiben_clicked();
			}

			private void btnInsLabelSchreiben_clicked() {

				label_bsptext.setText(txtHierBitteText.getText());
			}
		});
		btnInsLabelSchreiben.setBounds(10, 215, 168, 23);
		contentPane.add(btnInsLabelSchreiben);

		JButton btnTextImLabel = new JButton("Text im Label l\u00F6schen");
		btnTextImLabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnTextImLabel_clicked();
			}

			private void btnTextImLabel_clicked() {

				label_bsptext.setText("");
			}
		});
		btnTextImLabel.setBounds(192, 215, 174, 23);
		contentPane.add(btnTextImLabel);

		JButton btnRotT = new JButton("Rot");
		btnRotT.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnRotT_clicked();
			}

			private void btnRotT_clicked() {
				label_bsptext.setForeground(Color.red);
			}
		});
		btnRotT.setBounds(10, 270, 112, 23);
		contentPane.add(btnRotT);

		JButton btnBlauT = new JButton("Blau");
		btnBlauT.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnBlauT_clicked();
			}

			private void btnBlauT_clicked() {
				label_bsptext.setForeground(Color.blue);
			}
		});
		btnBlauT.setBounds(132, 270, 112, 23);
		contentPane.add(btnBlauT);

		JButton btnSchwarzT = new JButton("Schwarz");
		btnSchwarzT.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnSchwarzT_clicked();
			}

			private void btnSchwarzT_clicked() {
				label_bsptext.setForeground(Color.black);
			}
		});
		btnSchwarzT.setBounds(254, 270, 112, 23);
		contentPane.add(btnSchwarzT);

		JButton button_SsizeUP = new JButton("+");
		button_SsizeUP.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnSsizeUP_clicked();
			}

			private void btnSsizeUP_clicked() {
				int s = label_bsptext.getFont().getSize();
				if(s<19) {s++;}
				label_bsptext
						.setFont(new Font(label_bsptext.getFont().getName(), label_bsptext.getFont().getStyle(), s));
			}
		});
		button_SsizeUP.setBounds(10, 329, 168, 23);
		contentPane.add(button_SsizeUP);

		JButton button_SsizeDOWN = new JButton("-");
		button_SsizeDOWN.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnSsizeDOWN_clicked();
			}

			private void btnSsizeDOWN_clicked() {
				int s = label_bsptext.getFont().getSize();
				if(s>1) { s--;}
				label_bsptext
						.setFont(new Font(label_bsptext.getFont().getName(), label_bsptext.getFont().getStyle(), s));
			}
		});
		button_SsizeDOWN.setBounds(192, 329, 174, 23);
		contentPane.add(button_SsizeDOWN);

		JButton btnLinksbuendig = new JButton("linksb\u00FCndig");
		btnLinksbuendig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnLinksbuendig_clicked();
			}

			private void btnLinksbuendig_clicked() {
				label_bsptext.setHorizontalAlignment(SwingConstants.LEFT);
			}
		});
		btnLinksbuendig.setBounds(10, 390, 112, 23);
		contentPane.add(btnLinksbuendig);

		JButton btnZentriert = new JButton("zentriert");
		btnZentriert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnZentriert_clicked();
			}

			private void btnZentriert_clicked() {
				label_bsptext.setHorizontalAlignment(SwingConstants.CENTER);
			}
		});
		btnZentriert.setBounds(132, 390, 112, 23);
		contentPane.add(btnZentriert);

		JButton btnRechtsbuendig = new JButton("rechtsb\u00FCndig");
		btnRechtsbuendig.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnRechtsbuendig_clicked();
			}

			private void btnRechtsbuendig_clicked() {
				label_bsptext.setHorizontalAlignment(SwingConstants.RIGHT);
			}
		});
		btnRechtsbuendig.setBounds(254, 390, 112, 23);
		contentPane.add(btnRechtsbuendig);

		JButton btnExit = new JButton("EXIT");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnExit_clicked();
			}

			private void btnExit_clicked() {
				System.exit(0);
			}
		});
		btnExit.setBounds(10, 446, 356, 23);
		contentPane.add(btnExit);
	}
}
